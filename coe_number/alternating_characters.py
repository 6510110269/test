def alternatingCharacters(s):
    lst = 0
    for i in range(len(s)-1):
        if s[i] == s[i+1]:
            lst += 1
    return lst