def funnyString(s):
    lst = []
    for i in range(len(s)-1):
        lst.append(abs(ord(s[i])-ord(s[i+1])))
    if lst==lst[::-1]:
        return 'Funny'
    return  'Not Funny'