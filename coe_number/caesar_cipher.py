def caesarCipher(s, k):
    Answer=""
    for i in s:
        if i in "abcdefghijklmnopqrstuvwxyz":
            Answer +=chr(97+(ord(i)-97+k)%26)
        elif i in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            Answer +=chr(65+(ord(i)-65+k)%26)
        else: 
            Answer += i
    return Answer   