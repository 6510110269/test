from coe_number.caesar_cipher import caesarCipher
import unittest

class caesar_cipher(unittest.TestCase):
    def test_give_s_middleOutz_k_2_is_cd(self):
        s="middle-Outz"
        k=2
        is_cd = caesarCipher(s,k)
        self.assertEqual(is_cd, "okffng-Qwvb")
    
    def test_give_s_aa_k_0_is_aa(self):
        s='aa'
        k=0
        is_aa = caesarCipher(s,k)
        self.assertEqual(is_aa, 'aa')
    
    def test_give_s_aa_k_26_is_aa(self):
        s='aa'
        k=26
        is_aa = caesarCipher(s,k)
        self.assertEqual(is_aa, 'aa')    
    def test_give_s__k_1_is_aa(self):
        s='/*+'
        k=1
        is_ = caesarCipher(s,k)
        self.assertEqual(is_, '/*+')    