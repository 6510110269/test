from coe_number.funny_string import funnyString
import unittest

class funny_stringtest(unittest.TestCase):
    def test_give_abde_is_funny(self) :
        Aphabet_lst_1=['a','b','d','e']
        is_funny = funnyString(Aphabet_lst_1)
        self.assertEqual(is_funny, 'Funny')
        
    def test_give_bcxy_is_Notfunny(self) :
        Aphabet_lst_2=['b','c','x','z']
        is_Notfunny = funnyString(Aphabet_lst_2)
        self.assertEqual(is_Notfunny, 'Not Funny')