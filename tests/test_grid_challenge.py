from coe_number.grid_challenge import gridChallenge
import unittest

class grid_challenge_test(unittest.TestCase) :
    def test_give_ebacd_fghij_olmkn_trpqs_xywuv_is_YES(self) :
        grid=['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "YES")
    
    def test_give_abc_lmp_qrt_is_YES(self) :
        grid=['abc', 'lmp', 'qrt']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "YES")

    def test_give_mpxz_abcd_wlmf_is_YES(self) :
        grid=['mpxz', 'abcd', 'wlmf']
        is_YES = gridChallenge(grid)
        self.assertEqual(is_YES, "NO")