from coe_number.alternate import alternate
import unittest
class alternate_test(unittest.TestCase):
    def test_give_beabeefeab_is_5(self):
        s='beabeefeab'
        is_5 = alternate(s)
        self.assertEqual(is_5, 5)

    def test_give_asdcbsdcagfsdbgdfanfghbsfdab_is_8(self):
        s='asdcbsdcagfsdbgdfanfghbsfdab'
        is_8 = alternate(s)
        self.assertEqual(is_8, 8)