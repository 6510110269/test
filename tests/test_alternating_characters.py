from coe_number.alternating_characters import alternatingCharacters
import unittest

class alternating_characters(unittest.TestCase):
    def test_give_AAAA_is_3(self) :
        s='AAAA'
        is_3 = alternatingCharacters(s)
        self.assertEqual(is_3, 3)

    def test_give_ABAB_is_0(self) :
        s='ABAB'
        is_0 = alternatingCharacters(s)
        self.assertEqual(is_0, 0)
    
    def test_give_AAABBB_is_4(self) :
        s='AAABBB'
        is_4 = alternatingCharacters(s)
        self.assertEqual(is_4, 4)
